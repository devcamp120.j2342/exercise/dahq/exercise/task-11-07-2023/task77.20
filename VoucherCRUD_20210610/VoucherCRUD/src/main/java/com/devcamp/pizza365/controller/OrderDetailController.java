package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.entity.OrderDetail;
import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.repository.OrderDetailRepository;
import com.devcamp.pizza365.repository.OrderRepository;
import com.devcamp.pizza365.repository.ProductRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class OrderDetailController {
    @Autowired
    OrderDetailRepository orderDetailRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    ProductRepository productRepository;

    @GetMapping("/orderDetail")

    public List<OrderDetail> getAllOrderDetail() {
        return orderDetailRepository.findAll();
    }

    @GetMapping("/orderDetail/{id}")
    public OrderDetail getOrderDetailById(@PathVariable("id") int id) {
        return orderDetailRepository.findById(id).get();
    }

    @PostMapping("/order/{idOrder}/product/{idProduct}/orderDetail")
    public ResponseEntity<OrderDetail> createOrderDetail(@PathVariable("idOrder") int idOrder,
            @PathVariable("idProduct") int idProduct, @RequestBody OrderDetail pOrderDetail) {
        try {
            Optional<Order> orderData = orderRepository.findById(idOrder);
            Optional<Product> productData = productRepository.findById(idProduct);
            OrderDetail orderDetailData = new OrderDetail();
            orderDetailData.setQuantityOrder(pOrderDetail.getQuantityOrder());
            orderDetailData.setPriceEach(pOrderDetail.getPriceEach());
            orderDetailData.setOrder(orderData.get());
            orderDetailData.setProduct(productData.get());
            return new ResponseEntity<>(orderDetailRepository.save(orderDetailData), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/orderDetail")
    public ResponseEntity<OrderDetail> createOrderDetaul(@RequestBody OrderDetail pOrderDetail) {
        return new ResponseEntity<>(orderDetailRepository.save(pOrderDetail), HttpStatus.CREATED);
    }

    @PutMapping("/orderDetail/{id}")
    public ResponseEntity<OrderDetail> updateOrderDetail(@PathVariable("id") int id,
            @RequestBody OrderDetail pOrderDetail) {
        try {
            Optional<OrderDetail> orderDetailData = orderDetailRepository.findById(id);
            if (orderDetailData.isPresent()) {
                orderDetailData.get().setQuantityOrder(pOrderDetail.getQuantityOrder());
                orderDetailData.get().setPriceEach(pOrderDetail.getPriceEach());
                return new ResponseEntity<>(orderDetailRepository.save(orderDetailData.get()), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/orderDetail/{id}")
    public ResponseEntity<OrderDetail> deleteOrderDetail(@PathVariable("id") int id) {
        orderDetailRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
