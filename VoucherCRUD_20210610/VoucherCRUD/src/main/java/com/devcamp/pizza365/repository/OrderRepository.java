package com.devcamp.pizza365.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.entity.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {

	List<Order> findByCustomerId(int customerId);

	Optional<Order> findByIdAndCustomerId(int id, int customerId);

	Order findByOrderDate(Date orderDate);
	// Optional<CRegion> findByRegionCodeAndCountryCountryCode(String
	// countryCode,String regionCode);
}
